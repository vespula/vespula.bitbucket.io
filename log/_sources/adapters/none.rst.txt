None Adapter
============

This adapter can be used to disable logging without making any changes to your code. Simply change the adapter.

.. code-block:: php
    
    <?php
    use Vespula\Log\Log;
    use Vespula\Log\Adapter\None NoLogger;

    $log_adapter = new NoLogger();
    $log = new Log($log_adapter);