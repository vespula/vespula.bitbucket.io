Generic Decorator
=================

The Generic decorator creates a basic unordered list that you can 
apply your own CSS to. It does not use any of the ``aria-label`` text. 

.. code-block:: php

    <?php
    use Vespula\Paginator\Paginator;
    use Vespula\Paginator\Decorator\Generic;
    use Zend\Diactoros\Uri;

    $decorator = new Generic();
    $paginator = new Paginator($decorator);
    $uri = new Uri('http://example.com');

    $page = 2;
    $total = 234;

    echo $paginator->paginate($page, $total, $uri); 

Sample output
--------------

.. code-block:: html

    <ul class="pagination">
        <li><a href="http://example.com?page=1">First</a></li>
        <li><a href="http://example.com?page=1">Previous</a></li>
        <li><a href="http://example.com?page=1">1</a></li>
        <li class="active"><a href="http://example.com?page=2">2</a></li>
        <li><a href="http://example.com?page=3">3</a></li>
        <li><a href="http://example.com?page=4">4</a></li>
        <li><a href="http://example.com?page=5">5</a></li>
        <li><a href="http://example.com?page=6">6</a></li>
        <li><a href="http://example.com?page=7">7</a></li>
        <li><a href="http://example.com?page=8">8</a></li>
        <li><a href="http://example.com?page=9">9</a></li>
        <li><a href="http://example.com?page=10">10</a></li>
        <li><a href="http://example.com?page=3">Next</a></li>
        <li><a href="http://example.com?page=24">Last</a></li>
    </ul>

Sample CSS
-----------

The following CSS will produce pagination that looks like this:

.. image:: ../_static/generic.png

.. code-block:: css

    html {
        font-family: sans-serif;
        font-size: 10pt;
    }

    ul.pagination {
        display: inline-block;
        padding-left: 0;
        background-color: #efefef;
        border-left: 1px solid #ccc;
    }

    ul.pagination > li {
        display: inline;

    }

    ul.pagination a {
        text-decoration: none;
        color: #333;
        padding: 10px;
        border: 1px solid #ccc;
        border-left: 0;
        float: left;
    }
    ul.pagination li.active > a {
        background-color: #666;
        color: #fff;
        cursor: default;
    }
